<!DOCTYPE html>
<html>
<head>
<title>jqGird in Servlet</title>

<link rel="stylesheet" href="../css/jquery-ui.css">
<link rel="stylesheet" href="../css/ui.jqgrid.css">

<script src="../javascript/jquery-1.7.1.js"></script>
<script src="../javascript/i18n/grid.locale-en.js"></script>
<script src="../javascript/jquery.jqGrid.src.js"></script>
<script src="../javascript/jquery-ui.js"></script>

<script src="getJqGridData.js"></script>

</head>
<body>
        <table id="list">
                <tr>
                        <td />
                </tr>
        </table>
        <div id="pager"></div>
</body>
</html>