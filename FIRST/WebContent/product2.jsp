<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Product</title>

<link rel="stylesheet" type="text/css" media="screen" href="css/ui-lightness/jquery-ui-1.7.3.custom.css" />
<link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />


<script src="javascript/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="javascript/jquery.jqGrid.src.js" type="text/javascript"></script>
<script src="javascript/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="javascript/grid.common.js" type="text/javascript"></script>
<script src="javascript/grid.celledit.js" type="text/javascript"></script>
<script src="javascript/grid.formedit.js" type="text/javascript"></script>
<script src="javascript/jqDnR.js" type="text/javascript"></script>
<script src="javascript/jqModal.js" type="text/javascript"></script>
<script type="text/javascript">
        jqUtil={};
        jqUtil.data=new Array();    
    var onclickSubmitLocal = function(options,postdata) {
      //  data.push(postdata);
        var grid = $("#list").jqGrid();
        var grid_p = grid[0].p,
            idname = grid_p.prmNames.id,
            grid_id = grid[0].id,
            id_in_postdata = grid_id+"_id",
            rowid = postdata[id_in_postdata],
            addMode = rowid === "_empty",
            oldValueOfSortColumn;

        // postdata has row id property with another name. we fix it:
        if (addMode) {
            // generate new id
            var new_id = grid_p.records + 1;
            while ($("#"+new_id).length !== 0) {
                new_id++;
            }
            postdata[idname] = String(new_id);
        } else if (typeof(postdata[idname]) === "undefined") {
            // set id property only if the property not exist
            postdata[idname] = rowid;
        }
        delete postdata[id_in_postdata];

        // prepare postdata for tree grid
        if(grid_p.treeGrid === true) {
            if(addMode) {
                var tr_par_id = grid_p.treeGridModel === 'adjacency' ? grid_p.treeReader.parent_id_field : 'parent_id';
                postdata[tr_par_id] = grid_p.selrow;
            }

            $.each(grid_p.treeReader, function (i){
                if(postdata.hasOwnProperty(this)) {
                    delete postdata[this];
                }
            });
        }

        // decode data if there encoded with autoencode
        if(grid_p.autoencode) {
            $.each(postdata,function(n,v){
                postdata[n] = $.jgrid.htmlDecode(v); // TODO: some columns could be skipped
            });
        }

        // save old value from the sorted column
        oldValueOfSortColumn = grid_p.sortname === "" ? undefined: grid.jqGrid('getCell',rowid,grid_p.sortname);

        // save the data in the grid
        if (grid_p.treeGrid === true) {
            if (addMode) {
                grid.jqGrid("addChildNode",rowid,grid_p.selrow,postdata);
            } else {
                grid.jqGrid("setTreeRow",rowid,postdata);
            }
        } else {
            if (addMode) {
                grid.jqGrid("addRowData",rowid,postdata,options.addedrow);
            } else {
                grid.jqGrid("setRowData",rowid,postdata);
            }
        }

        if ((addMode && options.closeAfterAdd) || (!addMode && options.closeAfterEdit)) {
            // close the edit/add dialog
            $.jgrid.hideModal("#editmod"+grid_id,
                              {gb:"#gbox_"+grid_id,jqm:options.jqModal,onClose:options.onClose});
        }

        if (postdata[grid_p.sortname] !== oldValueOfSortColumn) {
            // if the data are changed in the column by which are currently sorted
            // we need resort the grid
            setTimeout(function() {
                grid.trigger("reloadGrid", [{current:true}]);
            },100);
        }

        // !!! the most important step: skip ajax request to the server
        this.processing = true;
        return {};
    }

        $(function(){
            $("#list").jqGrid({
                direction:"ltr",
                recordpos:"right",
                url:'jqgrid5e.php',
                datatype:'local',
                //cellsubmit:"clientarray",
                datatype: 'json',
                type: 'GET',
                colNames:['ID','Name', 'Gender'],
                colModel :[
                    {name:'id', index:'id', width:55, editable:true, edittype:'text',sorttype:'int'},
                    {name:'Name', index:'Name', width:90, editable:true, edittype:'text'},
                    {name:'Gender', index:'Gender', width:80, align:'right', editable:true, edittype:'text'}
                ],
                pager: '#pager',
                rowNum:10,
                width:1100,
                height:470,
                rowList:[10,20,30],
                sortname: 'id',
                sortorder: 'asc',
                loadonce:true,
                viewrecords: true,
                gridview: true,
                jsonReader : {
                    page: "page",
                    total: "total",
                    records: "records",
                    root:"d",
                    repeatitems: false,
                    id: "id"
                },
             
                onSelectRow: function(id){
                  
                },
                gridComplete:function(){
                    
                }
            });
            $("#list").jqGrid('navGrid','#pager',
            {add:true,edit:true,search:true,refresh:true},
           {//edit
    //recreateForm:true,
    jqModal:true,
    reloadAfterSubmit:false,
    closeOnEscape:true,
    savekey: [true,13],
    closeAfterEdit:true,
    //url:"clientArray",
    onclickSubmit:onclickSubmitLocal,
        /*
        function(params, postdata){
        }
        //save edit
        var p=params;
        var pt=postdata;
        return false;
    }
,*/
    beforeSubmit : function(postdata, formid) { 
        var p = postdata;
        var id=formid;
        var success=false;
        var message="continue";
        return[success,message]; 
    },    
    afterSubmit : function(response, postdata) 
    { 
        var r=response; 
        var p=postdata;
        var responseText=jQuery.jgrid.parse(response.responseText);
        var success=true;
        var message="continue1";
        return [success,message] 
    },
    afterComplete : function (response, postdata, formid) {        
        var responseText=jQuery.jgrid.parse(response.responseText);
        var r=response;
        var p=postdata;
        var f=formid;
    } 
    
},
{//add
    //recreateForm:true,
    jqModal:false,
    reloadAfterSubmit:false,
    savekey: [true,13],
    closeOnEscape:true,
    closeAfterAdd:true,
    //url:"clientArray",//jqgrid4s.php",
    onclickSubmit:onclickSubmitLocal,
    beforeSubmit : function(postdata, formid) { 
        var p = postdata;
        var id=formid;
        var success=true;
        var message="continue";
        return[success,message]; 
    },    
    afterSubmit : function(response, postdata) 
    { 
        var r=response; 
        var p=postdata;
        var responseText=jQuery.jgrid.parse(response.responseText);
        var success=true;
        var message="continue";
        return [success,message] 
    },
    afterComplete : function (response, postdata, formid) {        
        var responseText=jQuery.jgrid.parse(response.responseText);
        var r=response;
        var p=postdata;
        var f=formid;
    } 
}
            /*
             
             *editParam = {
                            editData:{myparam:function(){return "myval";}},
                            reloadAfterSubmit: true,
                            editCaption:'Edit Record',
                            bSubmit:'Save',
                            url:'<%=request.getContextPath()%>/CompanyJqGrid? q=1&action=addData',
                            closeAfterEdit:true,
                            viewPagerButtons:false
                        }*/
    ).jqGrid('navButtonAdd','#pager',
    {
        caption:"Save", 
        buttonicon:"ui-icon-save", 
        title:"Save All Records",
        onClickButton: function(x){ 
            var jsondata=JSON.stringify(jqUtil.data);
            $.ajax({
            type: 'POST',
            url: 'jqgrid5s.php',
            data: jsondata,
            dataType: 'json',
            success: function(data, textStatus, jqXHR){
                var d=data;
                var t=textStatus;
                var j=jqXHR;
                jqUtil.data=new Array();
                //alert("saved");
            },
            error:function(jqXHR, textStatus, errorThrown){
                var e=errorThrown;
                var t=textStatus;
                var j=jqXHR;
            }
            
        });
            //var ret = $("#list").getChangedCells('all'); 
            //jqgrid4s.php"
            //alert("Saved");
        }, 
        position:"first"
    });

    /* $.jgrid = {
            del : {
                            caption: "Delete1",
                            msg: "Delete selected record(s)123?",
                            bSubmit: "Delete2",
                            bCancel: "Cancel3"
                        }
        }


        parameters = {                    
            edit: {
                                addCaption: "Add Record1",
		editCaption: "Edit Record1",
		bSubmit: "Submit",
		bCancel: "Cancel",
		bClose: "Close",
		saveData: "Data has been changed! Save changes?",
		bYes : "Yes",
		bNo : "No",
		bExit : "Cancel"
            },
            editicon: "ui-icon-pencil",
            add: true,
            addicon:"ui-icon-plus",
            addtext:"add text",
            addtitle:"add_title",
            save: true,
            saveicon:"ui-icon-disk",
            cancel: true,
            cancelicon:"ui-icon-cancel",
            addParams : {useFormatter : false},
            editParams : {
                                addCaption: "Add Record",
		editCaption: "Edit Record",
		bSubmit: "Submit",
		bCancel: "Cancel",
		bClose: "Close",
		saveData: "Data has been changed! Save changes?",
		bYes : "Yes",
		bNo : "No",
		bExit : "Cancel"
            },
            
  view : {
    caption: "View Record1",
    bClose: "Close"
  }
        }
 *
 *
 *
 */

        });
</script>
</head>
<BODY bgcolor="white" topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 background="" onload="doOnLoad();">
 <form name="actionForm"  method="post">
<center>
<table width=1100 cellpadding=0 border=0 cellspacing=0>
	<tr>
		<td valign=top>

<TABLE WIDTH=1100 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
	<td><img src="images/rc.jpg" height="45%" width=40%" style="margin-right:100px;"/></td>
 	<td><h1 style="font-family:Castellar;color:#B22222;margin-left:50px;">&nbsp;&nbsp;&nbsp;RAGHAV CATERERS<h1></td>
	<td><img src="images/wedding_img.jpg" height="20%" width=25%" style="margin-left:160px;"/></td> 
	
	</TR>
	<TR>
			
			
		<TD bgcolor="#b11116" WIDTH=100% HEIGHT=25 valign=bottom COLSPAN=4>
			<table width=100% height=20 cellpadding=0 border=0 cellspacing=0>
							<tr>
							<td align=center width=100% valign=middle><font face="normal 13px Arial;" color="#ffffff" style="font-size='11px'">
							<a href="employee.jsp" style="color: white; text-decoration: none" class="link">EMPLOYEE</a> 
				&nbsp;|&nbsp; <a href="product.jsp" style="color: white; text-decoration: none" class="link">PRODUCT</a> 
				 &nbsp;|&nbsp; <a href="register.jsp" style="color: white; text-decoration: none" class="link">REGISTER</a> 
				 &nbsp;|&nbsp; <a href="viewup.aspx" style="color: white; text-decoration: none" class="link">TASK ASSIGNED</a> 
				 &nbsp;|&nbsp; <a href="massage_cm.html" style="color: white; text-decoration: none" class="link">MENUS</a> 
				 &nbsp;|&nbsp; <a href="mussoorie.html" style="color: white; text-decoration: none" class="link">ESTIMATION</a> 
				 &nbsp;|&nbsp; <a href="mussoorie.html" style="color: white; text-decoration: none" class="link">OVERALL DETAILS</a> 
				 	       </td>
				 	       </tr>
		    </table>
		</TD>
	</TR>
</TABLE>
		</td>
	</tr>
</table>

<table width=1100 bgcolor="FFF8DC" cellpadding=0 border=0 cellspacing=0 height=570px >
<tr valign="top" align="left">
	<td >	<br>

	&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="home_page.jsp" style="color: #B22222; text-decoration: none;" class="link"><font face="MONOSPACE" style="font-size='15px'">Home -></font></a>
	<font face="MONOSPACE" color="#B22222" style="font-size='20px'">
	<b>&nbsp;Register</b><hr color="#B22222" width=1050 size="0.1">
</td>
</tr>
<tr valign="top">
<td>
<table align="center" cellpadding="0" cellspacing="0" border="0" width="70%" >
		<tr>
		<table id="list"><tr><td/></tr></table>
<div id="pager"></div>
		</tr>
	
		
	</table>

</td>


	
	
</tr>

</table>

	<table>
<tr><td valign=middle bgcolor=#b11116 colspan=2 height=14 align=center width=1100>
<font face="normal 13px Arial" color=white>Designed & Developed by RK
</td></tr>
</table>
 </form>
</body>
</html>