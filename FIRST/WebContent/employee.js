
	function submitRequest()
	{  
		if(validateData())
		{
				var form = document.forms[0];
	  			new Ajax.Request('employee.do?cmd=save', {
		  		parameters : Form.serialize(form),
	  			method: 'post',
	  			onSuccess: successSaveFunc,
	  			onFailure:  failureFunc
	  			});
		}
	 
	}
	
	function deleteEmployee()
	{  
		if(validate())
		{
			var form = document.forms[0];
			new Ajax.Request('employee.do?cmd=delete', {
				parameters : Form.serialize(form),
				method: 'post',
				onSuccess: successdeleteFunc,
				onFailure:  failureFunc
			});
		}
	 
	}
	
	function searchEmployee()
	{  
		if(validate())
		{
			var form = document.forms[0];
			new Ajax.Request('employee.do?cmd=search', {
				parameters : Form.serialize(form),
				method: 'post',
				onSuccess: successSearchFunc,
				onFailure:  failureFunc
			});
		}
	 
	}

	function successSaveFunc(response){
	  
		 var content = response.responseText;
		    xmlDoc = new DOMParser().parseFromString(content,'text/xml');
		    var emp = xmlDoc.getElementsByTagName("Employee")[0];
		    document.getElementById("employeeCode").value = emp.getElementsByTagName("employeeId") [0].childNodes[0].nodeValue;
			document.getElementById("employeeSK").value = emp.getElementsByTagName("employeesSk") [0].childNodes[0].nodeValue;
			alert("Transaction completed successfully");
	}
	
	function successdeleteFunc(response){
	    	alert("Transaction completed successfully");
	    	clear();
	}
	
	function successSearchFunc(response){
		  
	    var content = response.responseText;
	    xmlDoc = new DOMParser().parseFromString(content,'text/xml');
	    var emp = xmlDoc.getElementsByTagName("Employee")[0];
	    alert(emp);
	    showMessage("params");
	    document.getElementById("employeeCode").value = emp.getElementsByTagName("employeeId") [0].childNodes[0].nodeValue;
		document.getElementById("employeeName").value = emp.getElementsByTagName("name") [0].childNodes[0].nodeValue;
		document.getElementById("phone1").value = emp.getElementsByTagName("phone1") [0].childNodes[0].nodeValue;
		document.getElementById("designation").value = emp.getElementsByTagName("designation") [0].childNodes[0].nodeValue;
		document.getElementById("salary").value = emp.getElementsByTagName("salary") [0].childNodes[0].nodeValue;
		document.getElementById("address").value = emp.getElementsByTagName("address") [0].childNodes[0].nodeValue;
		document.getElementById("employeeSK").value = emp.getElementsByTagName("employeesSk") [0].childNodes[0].nodeValue;
		document.getElementById("gender").value = emp.getElementsByTagName("gender") [0].childNodes[0].nodeValue;
		if(emp.getElementsByTagName("phone2") [0].childNodes[0].nodeValue){
			document.getElementById("phone2").value = emp.getElementsByTagName("phone2") [0].childNodes[0].nodeValue;
		}
	    
	}

	function failureFunc(response){
	     alert("Call is failed" );
	    
	}
	
	function newEmployee(){
		document.getElementById("employeeCode").value ="";
		document.getElementById("employeeName").value ="";
		document.getElementById("phone1").value ="";
		document.getElementById("phone2").value ="";
		document.getElementById("designation").value ="";
		document.getElementById("salary").value ="";
		document.getElementById("address").value ="";
		document.getElementById("employeeSK").value ="";
	}
	
	function clearEmployee(){
		document.getElementById("employeeCode").value ="";
		document.getElementById("employeeName").value ="";
		document.getElementById("phone1").value ="";
		document.getElementById("phone2").value ="";
		document.getElementById("designation").value ="";
		document.getElementById("salary").value ="";
		document.getElementById("address").value ="";
		document.getElementById("employeeSK").value ="";
	}
	
	function validateData(){

		if(document.getElementById("employeeName").value=="" ){
			alert("please enter the employee name");
			return false;
		}
		if(document.getElementById("phone1").value=="" ){
			alert("please enter the phone number1");
			return false;
		}
		if(document.getElementById("salary").value=="" ){
			alert("please enter the salary");
			return false;
		}
		if(document.getElementById("designation").value=="" ){
			alert("please enter the designation");
			return false;
		}
		if(document.getElementById("address").value=="" ){
			alert("please enter the address");
			return false;
		}
		return true;
		
	}
	
	function validate(){

		if(document.getElementById("employeeCode").value=="" ){
			alert("please enter the employee code");
			return false;
		}
		return true;
		
	}
	var popupWindow = null;
	function getEmployees(){
		
		var jspcall="lookup.jsp?name=getAllEmployees";
		var x = screen.width/2 - 700/2;
	    var y = screen.height/2 - 450/2;
	    popupWindow = window.open(jspcall, '','height=300,width=520,left='+x+',top='+y);
	  //  showPopWin('lookup.jsp', 400, 200, null);
	   
	}
	 function setValue(myVal) {
         document.getElementById("employeeCode").value = myVal;
     }

	 
	