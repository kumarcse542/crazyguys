<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>jqGird Demo</title>
 <script src="javascript/jquery-1.7.2.min.js"  type="text/javascript"></script>
        <script src="javascript/i18n/grid.locale-en.js"  type="text/javascript"></script>
        <script src="javascript/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="javascript/prototype.js"></script>
		<script type="text/javascript" src="javascript/JsonXml.js"></script>
        <script type="text/javascript" src="javascript/jquery-ui-1.8.20.custom.min.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ui-lightness/jquery-ui-1.7.3.custom.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />
          <link rel="stylesheet" type="text/css" media="screen" href="css/ui.multiselect.css" />

    
 
 
<script>

jQuery(document).ready(function() {
    $("#list").jqGrid({
            url : "http://localhost:8080/FIRST/GridServlet",
            datatype : "json",
            mtype : 'POST',
            colNames : [ 'Id', 'FirstName', 'LastName', 'City', 'State' ],
            colModel : [ {
                    name : 'id',
                    index : 'id',
                    width : 100
            }, {
                    name : 'firstName',
                    index : 'firstName',
                    width : 150,
                    editable : true
            }, {
                    name : 'lastName',
                    index : 'lastName',
                    width : 150,
                    editable : true
            }, {
                    name : 'city',
                    index : 'city',
                    width : 100,
                    editable : true
            }, {
                    name : 'state',
                    index : 'state',
                    width : 100,
                    editable : true
            } ],
            pager : '#pager',
            rowNum : 10,
            rowList : [ 10, 20, 30 ],
            sortname : 'invid',
            sortorder : 'desc',
            viewrecords : true,
            gridview : true,
            caption : 'Data Report',
            jsonReader : {
                    repeatitems : false,
            },
            editurl : "http://localhost:8080/FIRST/GridServlet"
    });
    jQuery("#list").jqGrid('navGrid', '#pager', {
            edit : true,
            add : true,
            del : true,
            search : true
    });
});
</script>
</head>
<body>
    <table id="list">
        <tr>
            <td />
        </tr>
    </table>
    <div id="pager"></div>
</body>
</html>