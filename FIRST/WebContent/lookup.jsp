<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JQGrid Special</title>     
  <script src="javascript/jquery-1.7.2.min.js"  type="text/javascript"></script>
        <script src="javascript/i18n/grid.locale-en.js"  type="text/javascript"></script>
        <script src="javascript/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ui-lightness/jquery-ui-1.7.3.custom.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/ui.multiselect.css" />
         
        <script>

        $( document ).ready(function() {
            console.log( "ready!" );
        $("#grps1").jqGrid({
           	url:'<%="http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()%>/GridServlet',
        	datatype: "json",
        	  colNames : [ 'Id', 'Code', 'Name'],
              colModel : [ {
                      name : 'id',
                      index : 'id',
                      width : 100
              }, {
                      name : 'code',
                      index : 'code',
                      width : 200,
                      editable : true
              }, {
                      name : 'name',
                      index : 'name',
                      width : 200,
                      editable : true
              } ],
              search:true,
              pager:'#pgrps1',
              jsonReader: {
          		repeatitems : false
          	},
              rowNum: 5,
              rowList: [5, 10, 20, 50],
              sortname: 'id',
              sortorder: 'asc',
              viewrecords: true,
              height: "100%",
              onSelectRow: function(rowid,iRow,iCol,e)
              {
              		var rowData = jQuery("#grps1").getRowData(rowid);
                    	var colData = rowData['id'];  
                    	   document.getElementById('cf1').value = colData;    
                    	   window.parent.setValue(colData);          	                                                                          
              }  ,                                                                   
              caption: "Search"
        });
        $("#grps1").jqGrid('navGrid','#pgrps1',{add:false,edit:false,del:false,search:true,refresh:true},
        		{},{},{},{multipleSearch:true, multipleGroup:true, showQuery: true});
        });

        function closeme()
		{
    	
			window.parent.hidePopWin()

		}
       function cancel()
		{
			document.getElementById('cf1').value = "";
            window.parent.setValue("");   
            window.parent.hidePopWin();
		}
        </script>
    </head>
    <body>
    <div>
    <input id="cf1" type="hidden" />
     <table id="grps1"></table>
<div id="pgrps1"></div>
<input name="Ok" type="button" value="Ok" id="Ok" class="btn" onclick="javascript:closeme();" />
<input name="Cancel" type="button" value="Cancel" id="Cancel" class="btn" onclick="javascript:cancel();" />
</div>
    </body>
</html>