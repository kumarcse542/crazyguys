
CREATE TABLE public.session (
                session_sk NUMERIC(15) NOT NULL,
                code VARCHAR(50) NOT NULL,
                description VARCHAR(50) NOT NULL,
                sort_order NUMERIC(5) NOT NULL,
                active_flag VARCHAR(1) NOT NULL,
                CONSTRAINT session_pk PRIMARY KEY (session_sk)
);
COMMENT ON TABLE public.session IS 'session like
morning tiffen
Lunch
Dinner
Break fast ext..,';


CREATE TABLE public.employees (
                employees_sk NUMERIC(15) NOT NULL,
                employee_id VARCHAR(50) NOT NULL,
                name VARCHAR(255) NOT NULL,
                address VARCHAR(2000) NOT NULL,
                phone1 VARCHAR(55) NOT NULL,
                phone2 VARCHAR(55) NOT NULL,
                date_of_join TIMESTAMP NOT NULL,
                gender VARCHAR(15) NOT NULL,
                designation VARCHAR(255) NOT NULL,
                salary NUMERIC(18,3) NOT NULL,
                CONSTRAINT employee_pk PRIMARY KEY (employees_sk)
);


CREATE TABLE public.product_stocks (
                product_sk NUMERIC(50) NOT NULL,
                code VARCHAR(50) NOT NULL,
                description VARCHAR(50) NOT NULL,
                amount NUMERIC(18,2) NOT NULL,
                quantity NUMERIC(18,3) NOT NULL,
                active_flag VARCHAR(1) NOT NULL,
                CONSTRAINT product_sk PRIMARY KEY (product_sk)
);


CREATE TABLE public.menu_items (
                event_menu_sk NUMERIC(50) NOT NULL,
                code VARCHAR(50) NOT NULL,
                description VARCHAR(2000) NOT NULL,
                classification VARCHAR(255) NOT NULL,
                sort_order NUMERIC(5) NOT NULL,
                active_flag VARCHAR(1) NOT NULL,
                CONSTRAINT menu_item_sk PRIMARY KEY (event_menu_sk)
);


CREATE TABLE public.customers (
                customer_sk NUMERIC(60) NOT NULL,
                customer_code VARCHAR(50) NOT NULL,
                customer_name VARCHAR(250) NOT NULL,
                customer_address VARCHAR(2000) NOT NULL,
                phone1 VARCHAR(55) NOT NULL,
                phone2 VARCHAR(55) NOT NULL,
                registered_date TIMESTAMP NOT NULL,
                CONSTRAINT customer_sk PRIMARY KEY (customer_sk)
);


CREATE TABLE public.orders (
                order_sk NUMERIC(15) NOT NULL,
                order_id VARCHAR(50) NOT NULL,
                customer_sk NUMERIC(60) NOT NULL,
                event_type VARCHAR(255) NOT NULL,
                venue VARCHAR(2000) NOT NULL,
                event_start_date TIMESTAMP,
                event_end_date TIMESTAMP,
                total_members NUMERIC(50),
                est_amount NUMERIC(18,3) NOT NULL,
                same_menu VARCHAR(1) NOT NULL,
                advance_amount NUMERIC(18,3) NOT NULL,
                CONSTRAINT order_sk PRIMARY KEY (order_sk)
);


CREATE TABLE public.purchased_items (
                purchased_items_sk NUMERIC(15) NOT NULL,
                order_sk NUMERIC(15) NOT NULL,
                description VARCHAR(50) NOT NULL,
                quantity NUMERIC(18,3) NOT NULL,
                price NUMERIC(18,3) NOT NULL,
                CONSTRAINT purchased_item_pk PRIMARY KEY (purchased_items_sk)
);


CREATE TABLE public.ord_labors (
                ord_labor_sk NUMERIC(15) NOT NULL,
                order_sk NUMERIC(15) NOT NULL,
                employees_sk NUMERIC(15) NOT NULL,
                CONSTRAINT ord_labors_pk PRIMARY KEY (ord_labor_sk)
);


CREATE TABLE public.ord_suplied_prod (
                suplied_prod_sk NUMERIC(15) NOT NULL,
                order_sk NUMERIC(15) NOT NULL,
                product_sk NUMERIC(50) NOT NULL,
                amount NUMERIC(18,2) NOT NULL,
                quantity NUMERIC(18,3) NOT NULL,
                CONSTRAINT suplied_product_pk PRIMARY KEY (suplied_prod_sk)
);


CREATE TABLE public.customer_menu (
                order_menu_sk NUMERIC(15) NOT NULL,
                order_sk NUMERIC(15) NOT NULL,
                event_menu_sk NUMERIC(50) NOT NULL,
                session_sk NUMERIC(15) NOT NULL,
                total_members NUMERIC(50),
                date TIMESTAMP NOT NULL,
                CONSTRAINT menu_sk PRIMARY KEY (order_menu_sk)
);


ALTER TABLE public.customer_menu ADD CONSTRAINT session_customer_menu_fk
FOREIGN KEY (session_sk)
REFERENCES public.session (session_sk)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ord_labors ADD CONSTRAINT employees_ord_labors_fk
FOREIGN KEY (employees_sk)
REFERENCES public.employees (employees_sk)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ord_suplied_prod ADD CONSTRAINT product_stocks_ord_suplied_prod_fk
FOREIGN KEY (product_sk)
REFERENCES public.product_stocks (product_sk)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.customer_menu ADD CONSTRAINT menu_items_customer_menu_fk
FOREIGN KEY (event_menu_sk)
REFERENCES public.menu_items (event_menu_sk)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.orders ADD CONSTRAINT customers_orderes_fk
FOREIGN KEY (customer_sk)
REFERENCES public.customers (customer_sk)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.customer_menu ADD CONSTRAINT orders_customer_menu_fk
FOREIGN KEY (order_sk)
REFERENCES public.orders (order_sk)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ord_suplied_prod ADD CONSTRAINT orders_ord_suplied_prod_fk
FOREIGN KEY (order_sk)
REFERENCES public.orders (order_sk)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.ord_labors ADD CONSTRAINT orders_ord_labors_fk
FOREIGN KEY (order_sk)
REFERENCES public.orders (order_sk)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.purchased_items ADD CONSTRAINT orders_purchased_items_fk
FOREIGN KEY (order_sk)
REFERENCES public.orders (order_sk)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
