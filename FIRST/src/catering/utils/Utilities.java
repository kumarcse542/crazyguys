package catering.utils;

import java.io.StringWriter;
import org.apache.commons.betwixt.io.BeanWriter;

public class Utilities {
	
	 public static String convertToXML(Object obj) throws Exception {
	       
	     StringWriter outputWriter = new StringWriter(); 
	     outputWriter.write("<?xml version='1.0' ?>");
	     BeanWriter beanWriter = new BeanWriter(outputWriter);
	         
	        beanWriter.getXMLIntrospector().getConfiguration().setAttributesForPrimitives(false);
	        beanWriter.getBindingConfiguration().setMapIDs(false);
	        beanWriter.enablePrettyPrint();
	        beanWriter.write(obj);
	        String xml = outputWriter.toString();
	        System.out.println(outputWriter.toString());
	        outputWriter.close();
	        
	     return xml;
	    }
}
