package catering.utils;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import catering.db.Employee;
  
public class HibernateUtil {  
  
 private static final SessionFactory sessionFactory = buildSessionFactory();  
  
 private static SessionFactory buildSessionFactory() {  
  try {  
	  
   return new AnnotationConfiguration().configure().buildSessionFactory();  
  } catch (Throwable ex) {  
   System.err.println("Initial SessionFactory creation failed." + ex);  
   throw new ExceptionInInitializerError(ex);  
  }  
 }  
  
 public static SessionFactory getSessionFactory() {  
  return sessionFactory;  
 }  
  
 public static void shutdown() {  
  getSessionFactory().close();  
 }  
 
  public static Employee getEmployee(Session session,String id){
	 Query q = session.createQuery("from Employee where employeeId =:id");
	 q.setParameter("id", id);
	 Employee employee = (Employee)q.list().get(0);
	 return employee;
 }
  
  @SuppressWarnings("unchecked")
  public static List<Employee> getAllEmployees(Session session){
	 Query q = session.createQuery("from Employee");
	List<Employee> employee = (ArrayList<Employee>)q.list();
	 return employee;
 }
 
}  
