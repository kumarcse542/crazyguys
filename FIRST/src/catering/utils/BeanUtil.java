package catering.utils;


import java.io.StringWriter;
import org.apache.commons.betwixt.io.BeanWriter;
import catering.db.Employee;

	public class BeanUtil {

	    /** 
	     * Create an example bean and then convert it to xml.
	     */
	    public static String beanToXML(Object emp) throws Exception {
	       
	     StringWriter outputWriter = new StringWriter(); 
	     outputWriter.write("<?xml version='1.0' ?>");
	     BeanWriter beanWriter = new BeanWriter(outputWriter);
	         
	        beanWriter.getXMLIntrospector().getConfiguration().setAttributesForPrimitives(false);
	        beanWriter.getBindingConfiguration().setMapIDs(false);
	        beanWriter.enablePrettyPrint();
	        beanWriter.write(emp);
	        String xml = outputWriter.toString();
	        System.out.println(outputWriter.toString());
	        outputWriter.close();
	        
	     return xml;
	    }
	    
	    public static void main(String[] args) throws Exception {  
	    	Employee emp = new Employee();
	    	beanToXML(emp);
	    }
	

}
