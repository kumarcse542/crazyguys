package catering.daoimpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import catering.dao.EmployeeDao;
import catering.db.Employee;
import catering.utils.HibernateUtil;

public class EmployeeDaoImpl implements EmployeeDao{

	@SuppressWarnings("rawtypes")
	public Employee save(Employee employee) {
		// TODO Auto-generated method stub
		System.out.println("===============================================");
		Session session=HibernateUtil.getSessionFactory().openSession();  
		Transaction tx = session.beginTransaction();
        session.beginTransaction();  
        Query query = session.createQuery("select max(employeeId) from Employee");
        List list = query.list();
        String id = "1";
        if(list.size()>0)
        {
        id = (String) list.get(0);
        	if(id!=null)
        	{
        		id = id.substring(1);
        		id = String.valueOf(new Integer(id)+1);
        	}
        	else{
        		id = "1";
        	}
        }
		employee.setEmployeeId("E"+id);
        session.save(employee);  
        tx.commit();
        session.close();
        return employee;
	}

	public Employee search(String id) {
		// TODO Auto-generated method stub
		System.out.println("--------------------------------------------------------");
		Session session=HibernateUtil.getSessionFactory().openSession();  
        session.beginTransaction();  
        Employee employee = HibernateUtil.getEmployee(session,id);
        session.close();
		return employee;
	}

	public void delete(String id) {
		// TODO Auto-generated method stub
		System.out.println("******************************************************");
		Session session=HibernateUtil.getSessionFactory().openSession();  
        session.beginTransaction();  
        Employee employee = HibernateUtil.getEmployee(session,id);
        session.delete(employee);
        session.close();
	}
	
	@SuppressWarnings("unchecked")
	public List<Employee> fetchAllEmployees(int offset, int limit) {
		System.out.println("--------------------------------------------------------");
		Session session=HibernateUtil.getSessionFactory().openSession();  
        session.beginTransaction();  
        Query q = session.createQuery("from Employee");
        q.setFirstResult(offset); 
        q.setMaxResults(limit);
   	    List<Employee> list = q.list();
        session.close();
		return list;
	}
}
