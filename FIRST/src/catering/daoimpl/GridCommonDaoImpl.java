package catering.daoimpl;

import org.hibernate.Session;

import catering.dao.GridCommonDao;
import catering.utils.HibernateUtil;

public class GridCommonDaoImpl implements GridCommonDao {

	public int getTotalCount(String value) {
		System.out.println("===============================================");
		Session session=HibernateUtil.getSessionFactory().openSession();  
        session.beginTransaction();  
        Long count = (Long) session.createQuery("select count(*) from "+value).uniqueResult();
        System.out.println("========count=============================>>>>>>>>>>>>"+count);
		return count.intValue();
	}
	
}
