package catering.db;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;
import java.math.BigDecimal;


/**
 * The persistent class for the employees database table.
 * 
 */
@Entity
@Table(name="employees")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@SequenceGenerator(name="EMPLOYEE_SEQUENCE_GENERATOR",sequenceName="employee_psk_seq")
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EMPLOYEE_SEQUENCE_GENERATOR")
	@Column(name="employees_sk")
	private long employeesSk;

	private String address;

	@Column(name="date_of_join")
	private Timestamp dateOfJoin;

	private String designation;

	@Column(name="employee_id")
	private String employeeId;

	private String gender;

	private String name;

	private String phone1;

	private String phone2;

	private BigDecimal salary;

	public Employee() {
	}

	public long getEmployeesSk() {
		return this.employeesSk;
	}

	public void setEmployeesSk(long employeesSk) {
		this.employeesSk = employeesSk;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Timestamp getDateOfJoin() {
		return this.dateOfJoin;
	}

	public void setDateOfJoin(Timestamp dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getEmployeeId() {
		return this.employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone1() {
		return this.phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return this.phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public BigDecimal getSalary() {
		return this.salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

}