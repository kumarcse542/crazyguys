package catering.helper;

import java.math.BigDecimal;

import catering.db.Employee;
import catering.form.EmployeeForm;

public class EmployeeHelper {

	public Employee addEmployeeDetails(EmployeeForm employeeForm){
		 Employee employee = new Employee();
         employee.setName(employeeForm.getEmployeeName());
         employee.setSalary(new BigDecimal(employeeForm.getSalary()));
         employee.setEmployeeId(employeeForm.getEmployeeCode());
         employee.setAddress(employeeForm.getAddress());
         employee.setDesignation(employeeForm.getDesignation());
         employee.setGender(employeeForm.getGender());
         employee.setPhone1(employeeForm.getPhone1());
         employee.setPhone2(employeeForm.getPhone2());
         
         return employee;
	}

}
