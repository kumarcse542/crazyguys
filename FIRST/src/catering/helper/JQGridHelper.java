package catering.helper;

import java.util.ArrayList;
import java.util.List;

import catering.db.Employee;
import catering.form.JqGridModel;
import catering.logic.EmployeeLogic;
import catering.logic.GridCommonLogic;

public class JQGridHelper {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List getGrid(String value, int offset, int limit) {
		List list = new ArrayList();
		if(value.equalsIgnoreCase("Employee")){
			EmployeeLogic logic = new EmployeeLogic();
			List<Employee> employeeList= logic.fetchAllEmployees(offset,limit);
			
			for(Employee employee : employeeList){
				JqGridModel gridModel = new JqGridModel();
				 gridModel.setId(employee.getEmployeesSk());
	             gridModel.setCode(employee.getEmployeeId());
	             gridModel.setName(employee.getName());
	             list.add(gridModel);
			}
			
			
			
		}
		return list;
	}

	public int getTotalCount(String value) {
		int count = 0;
		if(value.equalsIgnoreCase("Employee")){
			GridCommonLogic logic = new GridCommonLogic();
			count = logic.getTotalCount(value);
		}
		return count;
	}

	
}
