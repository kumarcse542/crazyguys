package catering.logic;

import catering.dao.GridCommonDao;
import catering.daoimpl.GridCommonDaoImpl;

public class GridCommonLogic {
	
	public int getTotalCount(String value){
		GridCommonDao dao = new GridCommonDaoImpl();
		int count = dao.getTotalCount(value);
		return count;
		
	}

}
