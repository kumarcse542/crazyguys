package catering.logic;


import java.util.List;

import catering.dao.EmployeeDao;
import catering.daoimpl.EmployeeDaoImpl;
import catering.db.Employee;

public class EmployeeLogic {
	
	public Employee save(Employee employee){
		EmployeeDao dao = new EmployeeDaoImpl();
		employee = dao.save(employee);
		System.out.println("saved");
		employee = dao.search(employee.getEmployeeId());
		return employee;
	}
	
    public Employee search(String id){
		EmployeeDao dao = new EmployeeDaoImpl();
		Employee employee = dao.search(id);
		return employee;
	}
    
    public void delete(String id){
  		EmployeeDao dao = new EmployeeDaoImpl();
  		dao.delete(id);

  	}

	public List<Employee> fetchAllEmployees(int offset, int limit) {
		EmployeeDao dao = new EmployeeDaoImpl();
		List<Employee> employee = dao.fetchAllEmployees(offset,limit);
		return employee;
	}

}
