package catering.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import catering.helper.JQGridHelper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
 
 
@SuppressWarnings("serial")
public class JQGridServlet extends HttpServlet {
    
     
    @SuppressWarnings("rawtypes")
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {        
        PrintWriter out = response.getWriter();
         try {
        	 
        	 JQGridHelper helper = new JQGridHelper();
             String rows = request.getParameter("rows");
             String page = request.getParameter("page");
             int pageLoad = Integer.parseInt(page);
             int rowsLoad = Integer.parseInt(rows);
             int totalPages = 0;
             int totalCount = helper.getTotalCount("Employee");
             
             if (rowsLoad > 0) {
	   		      if (totalCount % rowsLoad == 0)
	   		      	{
	   		    	  totalPages = totalCount / rowsLoad;
	   		      	} 
	   		      else 
	   		      	{
	   		    	  totalPages = (totalCount / rowsLoad) + 1;
	   		      	}
   		    }
      
             if (pageLoad > totalPages) 
            	 pageLoad=totalPages;
             int start = rowsLoad*pageLoad - rowsLoad; // do not put $limit*($page - 1)
             List list = helper.getGrid("Employee",start,rowsLoad);
             Gson gson = new GsonBuilder().setPrettyPrinting().create();
             String jsonArray = gson.toJson(list);
             jsonArray = "{\"page\":"+page+",\"total\":"+totalPages+",\"records\":"
                     + list.size() + ",\"rows\":" + jsonArray + "}";
             response.getWriter().print(jsonArray);  
        } finally {
            out.close();
        }
    } 
 
     
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        processRequest(request, response);
    } 
 
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        processRequest(request, response);
    }
}