package catering.action;


 
import java.io.Serializable;
 
/**
 * @author Sony
 * 
 */
public class GridModel implements Serializable {
 
    private int id;
    private String firstName;
    private String lastName;
    private String city;
    private String State;
    private String phoneNumber;
 
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
 
    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
 
    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }
 
    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }
 
    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
 
    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }
 
    /**
     * @param city
     *            the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }
 
    /**
     * @return the state
     */
    public String getState() {
        return State;
    }
 
    /**
     * @param state
     *            the state to set
     */
    public void setState(String state) {
        State = state;
    }
 
    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }
 
    /**
     * @param phoneNumber
     *            the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
 
}