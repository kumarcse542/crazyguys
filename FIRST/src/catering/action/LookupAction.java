package catering.action;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.hibernate.Query;
import org.hibernate.Session;

import catering.constants.QueryConstants;
import catering.db.Employee;
import catering.form.LookupForm;
import catering.helper.EmployeeHelper;
import catering.logic.EmployeeLogic;
import catering.utils.HibernateUtil;
import catering.utils.Utilities;

public class LookupAction extends Action{
	
	public ActionForward  execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception{
		System.out.println("----------------------------inside lookup action--------------------------------");
		String qryName = request.getParameter("name");
		LookupForm employeeForm = (LookupForm)form;
		search(response,qryName);
		return mapping.findForward("success"); 
		
	}
	
	public void search(HttpServletResponse response,String qryName) throws IOException{
		System.out.println("--------------------------------------------------------");
		Session session=HibernateUtil.getSessionFactory().openSession();  
        session.beginTransaction();  
    //    Employee employee = getAllEmployees(session,QueryConstants.getAllEmployees);
        session.close();
		
	}
	

}
