package catering.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GridServlet extends HttpServlet {
        private static final long serialVersionUID = 1L;

        protected void doPost(HttpServletRequest request,
                    HttpServletResponse response) throws ServletException, IOException {
System.out.println("------------------------------------");
                GridModel gridModel1 = new GridModel();
                gridModel1.setId(1);
                gridModel1.setFirstName("Mohaideen");
                gridModel1.setLastName("Jamil");
                gridModel1.setCity("Coimbatore");
                gridModel1.setState("TamilNadu");

                GridModel gridModel2 = new GridModel();
                gridModel2.setId(2);
                gridModel2.setFirstName("Ameerkhan");
                gridModel2.setLastName("Saffar");
                gridModel2.setCity("Thirunelveli");
                gridModel2.setState("Tamilnadu");

                List<GridModel> jqGridModels = new ArrayList<GridModel>();
                jqGridModels.add(gridModel1);
                jqGridModels.add(gridModel2);

                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                String jsonArray = gson.toJson(jqGridModels);
                jsonArray = "{\"page\":1,\"total\":\"2\",\"records\":"
                        + jqGridModels.size() + ",\"rows\":" + jsonArray + "}";

                System.out.println(jsonArray);

                response.getWriter().print(jsonArray);
        }
}