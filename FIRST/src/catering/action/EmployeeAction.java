package catering.action;

import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import catering.db.Employee;
import catering.form.EmployeeForm;
import catering.helper.EmployeeHelper;
import catering.logic.EmployeeLogic;
import catering.utils.Utilities;


public class EmployeeAction extends Action {
	
    Connection conn = null;
	
	String msg="success";
	
	public ActionForward  execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception{
		String cmdAction = request.getParameter("cmd");
		System.out.println("----------------------------inside action--------------------------------"+request.getParameter("cmd"));
		EmployeeForm employeeForm = (EmployeeForm)form;
		if(cmdAction.equals("save")){
			save(response, employeeForm);
		}
		else if(cmdAction.equals("search")){
			search(response, employeeForm);
		}
		else if(cmdAction.equals("fetchAll")){
			fetchAllEmployees(response, employeeForm);
		}
		else if(cmdAction.equals("delete")){
			delete(employeeForm);
		}
		return mapping.findForward("success"); 
		
	}
	
	public void save(HttpServletResponse response,EmployeeForm employeeForm) throws Exception{
		
		EmployeeHelper helper = new EmployeeHelper();
		EmployeeLogic logic = new EmployeeLogic();
		Employee employee = helper.addEmployeeDetails(employeeForm);
		employee = logic.save(employee);
		String xml = Utilities.convertToXML(employee);
		response.setContentLength(xml.length());
		PrintWriter out;
		out = response.getWriter();
		out.println(xml.toString());
		out.close();
		out.flush();	
	}
	
	public void search(HttpServletResponse response,EmployeeForm employeeForm) throws Exception{
		
		EmployeeLogic logic = new EmployeeLogic();
		Employee employee = logic.search(employeeForm.getEmployeeCode());
		String xml = Utilities.convertToXML(employee);
		response.setContentLength(xml.length());
		PrintWriter out;
		out = response.getWriter();
		out.println(xml.toString());
		out.close();
		out.flush();
		 
		
	}
	
	public EmployeeForm delete(EmployeeForm employeeForm){
		 EmployeeLogic logic = new EmployeeLogic();
		 logic.delete(employeeForm.getEmployeeCode());
		 return employeeForm;
		
	}
	
	
	
	 public static void main(String[] args) {  
		 EmployeeForm employeeForm = new EmployeeForm();
		 EmployeeLogic logic = new EmployeeLogic();
		 Employee employee = new Employee();
		 employee.setEmployeesSk(200);
		// System.out.println(Utilities.convertToXML(employee));
	
	 }
	 
	public void fetchAllEmployees(HttpServletResponse response,EmployeeForm employeeForm) throws Exception{
			
			EmployeeLogic logic = new EmployeeLogic();
			List<Employee> employee = logic.fetchAllEmployees(0, 0);
			String xml = Utilities.convertToXML(employee);
			response.setContentLength(xml.length());
			PrintWriter out;
			out = response.getWriter();
			out.println(xml.toString());
			out.close();
			out.flush();
			 
			
	}

}
