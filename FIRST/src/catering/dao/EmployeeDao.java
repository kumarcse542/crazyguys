package catering.dao;

import java.util.List;

import catering.db.Employee;

public interface EmployeeDao {
	
	public Employee save(Employee employee);

	public Employee search(String id);

	public void delete(String id);

	public List<Employee> fetchAllEmployees(int offset, int limit);

}
